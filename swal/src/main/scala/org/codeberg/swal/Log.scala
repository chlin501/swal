package org.codeberg.swal

import java.nio.file.Path

sealed trait Log

object Log {
  import Unsigneds._

  final case class Segment(path: Path, firstIndex: Int)

  final case class DefaultLog(
      path: Path,
      options: Options,
      segments: Seq[Segment],
      firstIndex: UInt64,
      lastIndex: UInt64
  )

}
