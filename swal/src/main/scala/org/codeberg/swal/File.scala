package org.codeberg.swal

import org.codeberg.swal.Log.Segment

import java.nio.file.Path

sealed trait File
final case class Open(path: Path) extends File
final case class Close(path: Path) extends File
final case class Load(segment: Segment) extends File
