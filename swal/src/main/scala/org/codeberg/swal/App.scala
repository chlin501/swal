package org.codeberg.swal

import java.nio.file.Path
import java.nio.file.attribute.{
  FileAttribute,
  PosixFilePermission,
  PosixFilePermissions
}
import scala.jdk.CollectionConverters._

sealed trait WalException
case object Corrupt extends WalException
case object Closed extends WalException
case object NotFound extends WalException
case object OutOfOrder extends WalException
case object OutOfRange extends WalException

sealed trait WalFormat
case object Binary extends WalFormat
case object Json extends WalFormat

final case class Options(
    noSync: Boolean = false,
    segmentSize: Int = 20,
    walFormat: WalFormat = Binary,
    segmentCacheSize: Int = 2,
    dirAttribute: FileAttribute[java.util.Set[PosixFilePermission]] =
      PosixFilePermissions.asFileAttribute(
        Set(
          PosixFilePermission.OWNER_READ,
          PosixFilePermission.OWNER_WRITE,
          PosixFilePermission.OWNER_EXECUTE,
          PosixFilePermission.GROUP_READ,
          PosixFilePermission.GROUP_EXECUTE
        ).asJava
      ),
    fileAttribute: FileAttribute[java.util.Set[PosixFilePermission]] =
      PosixFilePermissions.asFileAttribute(
        Set(
          PosixFilePermission.OWNER_READ,
          PosixFilePermission.OWNER_WRITE,
          PosixFilePermission.GROUP_READ
        ).asJava
      ),
)

object Unsigneds {
  import scala.CanEqual
  opaque type UInt64 = Long
  object UInt64 {

    /** A range between `0` and `(2^64)-1 (inclusive).`
      * @param value
      * @return
      *   either UInt64 or IllegalArgumentException
      */
    def apply(value: Int): Either[IllegalArgumentException, UInt64] =
      if (value < 0)
        Left(new IllegalArgumentException(s"$value is out of uint64 range!"))
      else Right(java.lang.Integer.toUnsignedLong(value))
  }
  given CanEqual[UInt64, UInt64] = CanEqual.derived
}

object App {

  def main(args: Array[String]): Unit = println("hey!")

}
