ThisBuild / version := "0.0.1"

ThisBuild / scalaVersion := "3.2.2"

lazy val root = (project in file(".")).aggregate(swal)

lazy val commons = project.settings(
  name := "commons",
  libraryDependencies ++= Seq(
    "org.slf4j" % "slf4j-api" % "2.0.6",
    "org.slf4j" % "slf4j-simple" % "2.0.6",
    "org.scalatest" %% "scalatest" % "3.2.15" % "test"
  )
)

lazy val swal = project
  .settings(
    name := "swal"
  )
  .dependsOn(commons)
